/*
Navicat MySQL Data Transfer

Source Server         : fernandez_rk
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : cmu_micm

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2019-02-13 00:45:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `location`
-- ----------------------------
DROP TABLE IF EXISTS `location`;
CREATE TABLE `location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `created_at` varchar(20) DEFAULT NULL,
  `updated_at` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of location
-- ----------------------------
INSERT INTO `location` VALUES ('1', 'balay sa ako crush', 'di ko magsaba kung aha', '2019-01-24', '2019-01-24');
INSERT INTO `location` VALUES ('3', 'ilang tulang', 'katong naay internetan', '2019-01-24', null);

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `fname` varchar(50) DEFAULT NULL,
  `mname` varchar(50) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `bday` varchar(30) DEFAULT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `uname` varchar(12) NOT NULL,
  `pword` varchar(12) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Rae Kenneth', 'Manrique', 'Fernandez', '1997-04-08', 'M', 'admin', 'admin', 'admin@admin.com');
INSERT INTO `users` VALUES ('2', 'Mario Victor Jose', 'Corpuz', 'Cabanag', '1993-02-01', 'M', 'mvjc', 'mvjc', 'mvjc@gmail.com');
INSERT INTO `users` VALUES ('3', 'Juleanne Rae', 'Paclibar', 'Fernandez', '1993-10-25', 'F', 'juyan', 'juyan', 'juyan@gmail.com');
