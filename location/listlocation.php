<?php 
    /** Check session if user is logged in
     * else redirect to landing page
     */
    session_start();
    if(empty($_SESSION) && !isset($_SESSION['uid'])) {
      header('Location: index.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Show Location</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css" type="text/css">
</head>
<body>
    <div class="container" style="margin-top:10%;">
    <a href="../index.php">Home</a>
        <h1 class="text-center">Location List</h1>
        <a href="registerlocation.php" class="btn btn-primary">Register Location</a>
        <hr>
        <table class="table table-hover table-bordered">
            <thead>
                <th>Location Name</th>
                <th>Description</th>
                <th>Date Created</th>
                <th>Action</th>
            </thead>
            <tbody>
                <?php 
                    require '../connection.php';

                    $table = 'location';

                    $query = "SELECT * FROM $table ORDER BY id DESC";

                    $result = mysqli_query($conn, $query) or die(mysqli_error_list($conn));

                    while ($record = mysqli_fetch_assoc($result))
                    {
                        echo '<tr>';
                        echo '<td>' . $record[name] . '</td>';
                        echo '<td>' . $record[description] . '</td>';
                        echo '<td>' . $record[created_at] . '</td>';
                        echo '<td><a href="updatelocation.php?id='. $record[id] .'" class="btn btn-warning" style="margin-right:3%;">Edit</a><a href="deletelocation.php?id='. $record[id] .'" class="btn btn-danger">Delete</a></td>';
                        echo "</tr>";
                    }
                ?>
                
            </tbody>
        </table>
    </div>
</body>
</html>