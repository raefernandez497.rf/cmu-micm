<?php 
    /** Check session if user is logged in
     * else redirect to landing page
     */
    session_start();
    if(empty($_SESSION) && !isset($_SESSION['uid'])) {
      header('Location: index.php');
    }
    if(!empty($_POST) && isset($_POST['btnRegister'])) {
        require '../connection.php';
        
        $name = $_POST['name'];
        $desc = $_POST['desc'];
        $created_at = date("Y-m-d");
        $table = 'location';

        $query="INSERT INTO $table (name, description, created_at) VALUES ('$name','$desc','$created_at')";

        mysqli_query($conn,$query) or die(mysqli_error($conn));

        echo "<script>alert('Register success!')</script>";
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register Location</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>
    <div class="container">
    <h1 class="text-center" style="margin-top:15%;">Register Location</h1>
        <div align="center">
            <form action="registerlocation.php" method="post">
                <div class="form-group">
                    <label for="name">Location Name</label>
                    <input type="text" name="name" id="name" class="form-control col-md-4">
                </div>
                <div class="form-group">
                    <label for="desc">Description</label>
                    <textarea name="desc" id="desc" rows="4" class="form-control col-md-4"></textarea>
                </div>
                <button type="submit" class="btn btn-primary col-md-4" name="btnRegister">
                Register</button>
            </form>
            <a href="listlocation.php">Show location list</a><br>
            <a href="../index.php">Home</a>
        </div>
    </div>
</body>
</html>