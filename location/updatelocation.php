<?php
    /** Check session if user is logged in
     * else redirect to landing page
     */
    session_start();
    if(empty($_SESSION) && !isset($_SESSION['uid'])) {
      header('Location: index.php');
    }
    require '../connection.php';

    /** Store the `location id` in a variable */
    $id = $_REQUEST['id'];
    
    /** Set database table to access */
    $table = 'location';

    /** For updating the location */
    if(!empty($_POST) && isset($_POST['btnUpdate'])) {

        $name = $_POST['name'];
        $desc = $_POST['desc'];
        $updated_at = date("Y-m-d");

        $query="UPDATE $table SET name='$name', description='$desc', updated_at='$updated_at' WHERE id='$id'";

        mysqli_query($conn, $query) or die(mysqli_error_list($conn));

        mysqli_close($conn);
        
        header('Location: listlocation.php');
}


    /** For getting the records of the location */

    /** Get the record using the id
     * in order to show the current value to inputs
     */
    $query = "SELECT * FROM $table WHERE id=$id";

    $result = mysqli_query($conn, $query) or die(mysqli_error_list($conn));

    if(mysqli_num_rows($result) == 1) {
        $record = mysqli_fetch_array($result);
        
        $cname = $record[name];
        $cdescription = $record[description];

        mysqli_close($conn);
    }
    else {
        mysqli_close($conn);
        echo'<script>alert("Error Fetching Records!");</script>';
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Update Location</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
</head>
<body>
    <div class="container">
    <h1 class="text-center" style="margin-top:15%;">Register Location</h1>
        <div align="center">
            <?php echo '<form action="updatelocation.php?id='. $id .'" method="post">'?>
                <div class="form-group">
                    <label for="name">Location Name</label>
                    <?php echo '<input type="text" name="name" id="name" class="form-control col-md-4" value="' . $cname . '">'?>
                </div>
                <div class="form-group">
                    <label for="desc">Description</label>
                    <textarea name="desc" id="desc" rows="4" class="form-control col-md-4"><?php echo $cdescription; ?></textarea>
                </div>
                <button type="submit" class="btn btn-primary col-md-4" name="btnUpdate">
                Update</button>
            </form>
            <a href="listlocation.php">Show location list</a><br>
            <a href="registerlocation.php">Register New Location</a>
            <a href="../index.php">Home</a>
        </div>
    </div>
</body>
</html>
