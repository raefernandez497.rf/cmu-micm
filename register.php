<?php 
    /** Check session if user is logged in
     * else redirect to landing page
     */
    session_start();
    if(empty($_SESSION) && !isset($_SESSION['uid'])) {
      header('Location: index.php');
    }
    if(!empty($_POST) && isset($_POST['btnRegister'])) {
        require 'connection.php';
        
        $uname = $_POST['uname'];
        $pword = $_POST['pword'];
        
        $table = 'users';

        $query="INSERT INTO $table (uname,pword) VALUES ('$uname','$pword')";

        mysqli_query($conn,$query) or die(mysqli_error($conn));

        echo "<script>alert('Register success!')</script>";
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register User Page</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css" type="text/css">
</head>
<body class="bg-light">

  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Register User</div>
      <div class="card-body">
        <form role="form" method="post" action="register.php">
          <div class="form-group">
            <label for="uname">Username</label>
            <input class="form-control" id="uname" name="uname" type="text" aria-describedby="unameHelp" placeholder="Enter username" required>
          </div>
          <div class="form-group">
            <label for="pword">Password</label>
            <input class="form-control" id="pword" name="pword" type="password" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-primary btn-block" name="btnRegister">
          <i class="fa fa-fw fa-sign-in"></i>Register</button>
        </form>
      </div>
    </div>
  </div>
</body>
</html>