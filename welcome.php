<?php 
    session_start();
    if(empty($_SESSION) && !isset($_SESSION['uid'])) {
      header('Location: index.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.2.1/css/bootstrap.min.css" type="text/css">
</head>
<body>
    <div class="text-center" style="margin-top:15%;">
        <h1>You're now logged in!</h1>
        <div class="form-group">
            <a href="location/registerlocation.php">Register Location</a><br>
            <a href="location/listlocation.php">Show Location List</a>
        </div>
        <a href="logout.php" class="btn btn-primary">Logout</a>
    </div>
</body>
</html>